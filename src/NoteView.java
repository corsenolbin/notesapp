import java.util.ArrayList;
import java.util.List;
import javafx.application.*;
import javafx.geometry.HPos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.stage.Stage;

public class NoteView extends Application {
	// Only getXxx() from model
	// Results go to Controller that then setXxx()
	
	private FlowPane notesFlow;
	private FlowPane menuFlow;
	private NoteController notesControl;
	private List<String> Tags;
	final BorderPane rootNode = new BorderPane();
	private int tagIndex;



	public void start(Stage myStage) {
			
		Scene myScene = new Scene(rootNode, 800, 600);		
		myStage.setScene(myScene);
		myStage.setTitle("Notes App");
		
		notesControl = new NoteController();
		
		notesFlow = new FlowPane(5, 5);
		notesFlow.setStyle("-fx-padding: 5;"
				+ "-fx-background-color: bisque;");
		
		Tags = new ArrayList<String>();
		
		setupMenu();
		
		rootNode.setTop(menuFlow);
		
		rootNode.setCenter(notesFlow);
		
		myStage.show();
		
				
	}
		
	void setupMenu() {
		menuFlow = new FlowPane(10, 10);
		
		menuFlow.setStyle("-fx-padding: 5; "
							+ "-fx-background-color: coral;");

		
		Button bCreate = new Button("New Note");
		Button bTags = new Button("Tags");
		
		bCreate.setOnAction((ae) -> notesControl.createNote(notesFlow));
		bTags.setOnAction((ae) -> setupTagSidePanel());
		
		menuFlow.getChildren().addAll(bCreate, bTags);
		
		
	}
	
	void setupTagSidePanel() {
		GridPane tagGrid = new GridPane();
		tagGrid.setStyle("-fx-margin: 10;"
				+ "-fx-padding: 10;"
				+ "-fx-border-width: 0 0 0 3;"
				+ "-fx-border-color: #ff7f50;"
				+ "-fx-background-color: bisque;"
				+ "-fx-alignment: top-Center;");
		tagGrid.setMinWidth(200);
		
		
		Button bNewTag = new Button("New Tag+");
		Button bCreateTag = new Button("Create");
		GridPane.setHalignment(bCreateTag, HPos.RIGHT);
		TextField tNewTag = new TextField();
		
		tagIndex = 1;
		if(!Tags.isEmpty()) {
			for(String tag: Tags) {
				Label lTag = new Label(tag);
				tagGrid.getChildren().add(lTag);
			}
		}
		
		tagGrid.getChildren().add(bNewTag);
		
		bNewTag.setOnAction((ae) -> {
			tagGrid.getChildren().remove(bNewTag);
			tagGrid.addColumn(1, tNewTag, bCreateTag);		
			tagIndex++;
			
		});
		
		bCreateTag.setOnAction((ae) -> {
			String tagText = tNewTag.getText();
			if(!tagText.isEmpty()) {
				Tags.add(tagText);
				tagGrid.addColumn(1, new Label(tagText));
				tNewTag.clear();
			}
			tagGrid.getChildren().removeAll(bCreateTag, tNewTag);
			tagGrid.addColumn(1, bNewTag);
			
		});
		
		rootNode.setRight(tagGrid);
	}
	
	
	

}

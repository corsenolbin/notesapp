import javafx.scene.control.*;
import javafx.scene.layout.*;

class NoteController {
	
	void createNote(FlowPane flow ) {
		
		NoteDialogs dialog = new NoteDialogs();
		Note note = dialog.createNoteDialog();
		
		if(note == null)
			return;
		
		VBox noteBox = new VBox(0);
		noteBox.setStyle("-fx-padding: 5;" 
				+ "-fx-background-color: coral;");
			
		Label lTitle = new Label("Title: " + note.getTitle());
		Label lNote = new Label(note.getNote());
		Button bEdit = new Button("Edit");
		
		bEdit.setOnAction((ae) -> {
			dialog.editNoteDialog(note);
			lNote.setText(note.getNote());
			lTitle.setText(note.getTitle());
						});
		
		noteBox.getChildren().addAll(lTitle, lNote, bEdit);
		
		flow.getChildren().add(noteBox);
		
	}
}
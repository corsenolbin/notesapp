import java.util.ArrayList;

public class Note {
	private String note;
	private String title;
	private ArrayList<String> tags;
	
	
	Note (String title, String note) {
		this.note = note;
		this.title = title;
		this.tags = new ArrayList<String>();		
	}
	
	Note (String title, String note, ArrayList<String> tags) {
		this.note = note;
		this.title = title;
		this.tags = tags;
	}
	
	void Print() {
		System.out.println("Note: " + getTitle());
		System.out.println(getNote());
		PrintTags();
		System.out.println();
	}
	
	void PrintTags() {
		for (String tag: tags) {
			System.out.print(tag + ", ");
		}
		System.out.println();
	}
	
	boolean HasValues() {
		if (note.isEmpty() || title.isEmpty())
			return false;
		return true;
	}
	
	String getNote() {
		return note;
	}
	
	void setNote(String newNote) throws Exception{
		if(newNote.length() < 250) {
			note = newNote;
		}
		else {
			throw new Exception("Length of new note was too long.");
		}
	}
	
	ArrayList<String> getTags() {
		return tags;
	}
	
	void setTags(ArrayList<String> tags) {
		this.tags = tags;
	}
	
	
	String getTitle() {
		return title;
	}
	
	void setTitle(String newTitle) {
		this.title = newTitle;
	}

}

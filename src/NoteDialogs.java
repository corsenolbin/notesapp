import java.util.Optional;

import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

class NoteDialogs {
	
	Note createNoteDialog() {
		Dialog<Note> dialog = new Dialog<Note>();
		
		
		GridPane createNotePane = new GridPane();
		
		TextField tfTitle = new TextField();
		tfTitle.setPrefColumnCount(20);
		TextArea tfNote = new TextArea();
		tfNote.setPrefColumnCount(18);
		tfNote.setPrefRowCount(5);
		
		createNotePane.add(tfTitle, 1, 1);
		createNotePane.add(tfNote, 1, 2);
		dialog.getDialogPane().setContent(createNotePane);
		
		ButtonType buttonTypeOk = new ButtonType("Okay", ButtonData.OK_DONE);
		dialog.getDialogPane().getButtonTypes().add(buttonTypeOk);
		
		dialog.setResultConverter((b) -> b == buttonTypeOk ? new Note(tfTitle.getText(), tfNote.getText()) : null);
		
		Optional<Note> result = dialog.showAndWait();
		
		if (result.isPresent() && result.get().HasValues()) {
			return result.get();
		}
			return null;
		
	}
	
	void editNoteDialog(Note note) {
		Stage editStage = new Stage();
				
		GridPane editNotePane = new GridPane();
		editNotePane.setStyle("-fx-spacing: 10;"
				+ "-fx-padding: 5;");
		Scene editScene = new Scene(editNotePane, 200, 300);
		
		String ogTitle = note.getTitle();
		String ogNote = note.getNote();
		
		TextField tfTitle = new TextField(ogTitle);
		tfTitle.setPrefColumnCount(20);
		TextArea tfNote = new TextArea(ogNote);
		tfNote.setPrefColumnCount(18);
		tfNote.setPrefRowCount(5);
		
		Button bApply = new Button("Apply");
		bApply.setAlignment(Pos.BOTTOM_RIGHT);
		Button bCancel = new Button("Cancel");
		bCancel.setAlignment(Pos.BOTTOM_RIGHT);
		
		bApply.setOnAction((ae) -> {
			try {
				note.setNote(tfNote.getText());
				note.setTitle(tfTitle.getText());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			finally {
				editStage.close();
			}
		});
		
		editNotePane.addColumn(0, tfTitle, tfNote, bApply, bCancel);
		editStage.setScene(editScene);
		
		editStage.showAndWait();
		

	}
}
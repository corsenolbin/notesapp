import java.util.ArrayList;
import java.util.Collection;

public class Notes {
	
	private ArrayList<Note> notes;
	
	Notes() {
		notes = new ArrayList<Note>();
	}
	
	Notes(ArrayList<Note> notes) {
		this.notes = notes;
	}
	
	int Length() {
		return notes.size();
	}
	
	void Add(Note note) {
		notes.add(note);		
	}
	
	void AddMany(Collection<Note> notes) {
		notes.addAll(notes);
	}
	
	void PrintAll() {
		for (Note note: notes) {
			note.Print();
		}
	}

}
